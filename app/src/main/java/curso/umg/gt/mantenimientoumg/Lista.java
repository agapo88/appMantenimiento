package curso.umg.gt.mantenimientoumg;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Lista extends AppCompatActivity {
    private ListView listaV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        listaV = (ListView) findViewById( R.id.listaPersonas );

        UsuarioSQLite db = new UsuarioSQLite(this);
        List<Empleado> personas = db.getEmpleados();
        String concPer = "";

        List<String> listaP = new ArrayList<String>();

        for (Empleado pers : personas) {

            String item = "NOMBRES: " + pers.getNombres() +
                    "\nAPELLIDOS " + pers.getApellidos() +
                    "\nIDPERFIL " + pers.getIdPerfil() +
                    "\nPERFIL " + pers.getPerfil();

            listaP.add(item);
            concPer += item;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>( this, android.R.layout.simple_list_item_1,listaP );
        listaV.setAdapter( adapter );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lista, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
