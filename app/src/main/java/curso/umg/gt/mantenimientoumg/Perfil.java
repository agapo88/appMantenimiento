package curso.umg.gt.mantenimientoumg;

/**
 * Created by USUARIO on 27/07/2017.
 */

public class Perfil {
    private int idPerfil;
    private String perfil;

    public Perfil() {
    }

    public int getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(int idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
}
