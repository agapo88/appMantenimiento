package curso.umg.gt.mantenimientoumg;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private Button btnIngresar,btnPersonas,btnPerfiles, btnHora;
    private EditText txtNombre,txtApellido,txtEdad;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre   = (EditText) findViewById(R.id.txtNombre);
        txtApellido = (EditText) findViewById(R.id.txtApellido);
        txtEdad     = (EditText) findViewById(R.id.txtEdad);
        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnPersonas = (Button) findViewById(R.id.btnPersonas);
        btnPerfiles = (Button) findViewById(R.id.btnPerfiles);
        btnHora =(Button) findViewById(R.id.btnHora);


        final UsuarioSQLite db = new UsuarioSQLite(this);

        final Empleado p = new Empleado();

        p.setNombres("JORGE");
        p.setApellidos("PEREZ");
        p.setIdPerfil(1);

        Empleado p1 = new Empleado();
        p1.setNombres("JUAN");
        p1.setApellidos("LOPEZ");
        p1.setIdPerfil(2);

        db.addEmpleado(p);
        db.addEmpleado(p1);




        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtNombre.getText().toString().equals("")){
                    Toast msj = Toast.makeText(MainActivity.this,"Error nombre muy corto.",Toast.LENGTH_SHORT);
                    msj.show();
                }

                else if (txtApellido.getText().toString().equals("")){
                    Toast msj = Toast.makeText(MainActivity.this,"Error apellido muy corto.",Toast.LENGTH_SHORT);
                    msj.show();
                }

                else if (txtEdad.getText().toString().equals("")){
                    Toast msj = Toast.makeText(MainActivity.this,"Error edad no valido.",Toast.LENGTH_SHORT);
                    msj.show();
                }

                else{
                    Empleado p = new Empleado();
                    p.setNombres(txtNombre.getText().toString());
                    p.setApellidos(txtApellido.getText().toString());
                    p.setIdPerfil(Integer.parseInt(txtEdad.getText().toString()));

                    txtNombre.setText("");
                    txtApellido.setText("");
                    txtEdad.setText("");

                    db.addEmpleado( p );

                    Toast msj = Toast.makeText(MainActivity.this,"Correcto",Toast.LENGTH_SHORT);
                    msj.show();
                }
            }
        });

        btnPersonas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,Lista.class);
                startActivity(i);
            }
        });

        btnPerfiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,Perfiles.class);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu ){
        getMenuInflater().inflate( R.menu.menu_principal, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ){
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);






    }

}