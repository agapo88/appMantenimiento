package curso.umg.gt.mantenimientoumg;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static curso.umg.gt.mantenimientoumg.R.id.btnIngresar;
import static curso.umg.gt.mantenimientoumg.R.id.txtNombre;

public class HorarioActivity extends AppCompatActivity {

    UsuarioSQLite datab;
    private TextView etiqueta;
    private EditText nombrec;
    private Spinner spinner1;
    private List<String> lista;
    private EditText edit1;
    Button btnHorario;
    private EditText txtLugar, txtHora, txtJornada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horario);

        final UsuarioSQLite db = new UsuarioSQLite(this);

        txtLugar   = (EditText) findViewById(R.id.txtLugar);
        txtHora   = (EditText) findViewById(R.id.txtHora);
        txtJornada  = (EditText) findViewById(R.id.txtJornada);


        this.etiqueta=(TextView)findViewById(R.id.etiqueta);
        this.nombrec=(EditText)findViewById(R.id.nombrec);

        spinner1= (Spinner) findViewById((R.id.spinner1));
        lista=new ArrayList<String>();
        datab=new UsuarioSQLite(this);
        this.nombrec.setText("");
        lista =datab.consultar2();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, lista);
        adapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);
        spinner1.setOnItemSelectedListener
                (new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                etiqueta.setText(parent.getItemAtPosition(position).toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*spinner=(Spinner)findViewById(R.id.spinner1);
        lista=new ArrayList<String>();*/
        this.edit1.setText("");


    }


        public void onClick(View v) {
            final UsuarioSQLite db = new UsuarioSQLite(this);

            if (txtLugar.getText().toString().equals("")){
                Toast msj = Toast.makeText(HorarioActivity.this,"Error nombre muy corto.",Toast.LENGTH_SHORT);
                msj.show();
            }

            else if (txtHora.getText().toString().equals("")){
                Toast msj = Toast.makeText(HorarioActivity.this,"Error al escribir.",Toast.LENGTH_SHORT);
                msj.show();
            }

            else if (txtJornada.getText().toString().equals("")){
                Toast msj = Toast.makeText(HorarioActivity.this,"Jornada no validad.",Toast.LENGTH_SHORT);
                msj.show();
            }

            else{
                Horario p = new Horario();
                p.setLugar(txtLugar.getText().toString());
                p.setHora(txtHora.getText().toString());
                p.setJornada(txtJornada.getText().toString());
                              //  p.setIdPerfil(Integer.parseInt(txtEdad.getText().toString()));

                txtLugar.setText("");
                txtHora.setText("");
                txtJornada.setText("");

                db.addHorario( p );

                Toast msj = Toast.makeText(HorarioActivity.this,"Correcto",Toast.LENGTH_SHORT);
                msj.show();
            }
        }




    }

