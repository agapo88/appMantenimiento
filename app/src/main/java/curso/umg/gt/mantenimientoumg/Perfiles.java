package curso.umg.gt.mantenimientoumg;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Perfiles extends AppCompatActivity {

    private ListView listaV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfiles);

        listaV = (ListView) findViewById(R.id.lstPerfiles);

        UsuarioSQLite db = new UsuarioSQLite(this);
        List<Perfil> perfiles = db.getPerfiles();
        String concPer = "";

        List<String> listaP = new ArrayList<String>();

        for (Perfil per : perfiles) {

            String item = "ID PERFIL: " + per.getIdPerfil() +
                    "\nPERFIL " + per.getPerfil();

            listaP.add(item);
            concPer += item;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>( this, android.R.layout.simple_list_item_1, listaP );
        listaV.setAdapter( adapter );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lista, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
