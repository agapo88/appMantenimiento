package curso.umg.gt.mantenimientoumg;

/**
 * Created by CLIENTE on 28/07/2017.
 */

public class Horario {
    private String lugar;
    private String hora;
    private String jornada;

    public Horario() {

    }

    public String getLugar() {return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getJornada() {
        return jornada;
    }

    public void setJornada(String jornada) {
        this.jornada = jornada;
    }
}
