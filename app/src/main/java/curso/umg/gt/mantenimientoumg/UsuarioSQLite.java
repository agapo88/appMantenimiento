package curso.umg.gt.mantenimientoumg;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USUARIO on 26/07/2017.
 */

public class UsuarioSQLite extends SQLiteOpenHelper {
    Horario datab;
    // SENTENCIA SQL
    String sqlEmpleado = "CREATE TABLE empleado ( idEmpleado INTEGER PRIMARY KEY AUTOINCREMENT, nombres TEXT, apellidos TEXT, idPerfil INTEGER, FOREIGN KEY(idPerfil) REFERENCES perfil(idPerfil) );";
    String sqlPerfil = "CREATE TABLE perfil ( idPerfil INTEGER PRIMARY KEY AUTOINCREMENT, perfil TEXT);";
    String sqlHorario= "CREATE TABLE horario ( idHorario INTEGER PRIMARY KEY AUTOINCREMENT, lugar TEXT, hora TEXT, jornada TEXT);";
    String sqlUsuario= "CREATE TABLE usuario ( codigo INTEGER PRIMARY KEY AUTOINCREMENT, usuario TEXT, contraseña TEXT);";

    //String sqlMantenimiento = "CREATE TABLE mantenimiento ( idMantenimiento INTEGER, mantenimiento TEXT, idEmpleado INT, FOREIGN KEY( idEmpleado ) REFERENCES empleado(idEmpleado) );";
    String sqlInsertPerfil = "INSERT INTO perfil( idPerfil, perfil ) VALUES( 1, 'Administrador'), ( 2, 'Supervisor'), ( 3, 'Encargado de Limpieza' );";
    String sqlInsertUsuario = "INSERT INTO usuario values( 'admin', 'admin' );";


    private static final int VERSION = 1;
    private static final String DATABASE = "mantenimientoDB";

    public UsuarioSQLite(Context context) {
        super(context, DATABASE, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL( sqlUsuario );
        db.execSQL( sqlPerfil );
        db.execSQL( sqlEmpleado );
        db.execSQL( sqlHorario );

       // db.execSQL( sqlMantenimiento );
        db.execSQL( sqlInsertPerfil );
        db.execSQL(sqlUsuario);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL( "DROP TABLE IF EXISTS empleado;" );
        //db.execSQL( "DROP TABLE IF EXISTS mantenimiento;" );
        db.execSQL( "DROP TABLE IF EXISTS perfil;" );
        //db.execSQL( "DELETE FROM perfil;" );
        db.execSQL(sqlUsuario);
        db.execSQL( sqlPerfil );
        db.execSQL( sqlEmpleado );
        db.execSQL( sqlHorario);

        //db.execSQL( sqlMantenimiento );
        db.execSQL( sqlInsertPerfil );

            }

    void addEmpleado(Empleado person){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("nombres", person.getNombres());
        values.put("apellidos",person.getApellidos());
        values.put("idPerfil", 1 );

        db.insert("Empleado", null, values);
        System.out.println("Guardado...");
        db.close();
    }

    // OBTENER LISTA DE PERFILES
    public List<Perfil> getPerfiles(){
        List<Perfil> listaPerfiles = new ArrayList<Perfil>();

        String sql = "SELECT * FROM perfil;";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql,null);

        if (cursor.moveToFirst()){
            do{
                Perfil per = new Perfil();
                per.setIdPerfil( cursor.getInt(0) );
                per.setPerfil(cursor.getString(1));

                listaPerfiles.add( per );
            }while (cursor.moveToNext());
        }

        return listaPerfiles;
    }


    public List<Empleado> getEmpleados(){
        List<Empleado> listaPersonas = new ArrayList<Empleado>();

        String sql = "SELECT nombres, apellidos, e.idPerfil, perfil FROM empleado e JOIN perfil p ON p.idPerfil = e.idPerfil;";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql,null);

        if (cursor.moveToFirst()){
            do{
                Empleado person = new Empleado();
                person.setNombres(cursor.getString(0));
                person.setApellidos(cursor.getString(1));
                person.setIdPerfil( cursor.getInt(2) );
                person.setPerfil(cursor.getString(3));

                listaPersonas.add( person );
            }while (cursor.moveToNext());
        }

        return listaPersonas;
    }

    public void addHorario(Horario horario){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("hora", horario.getLugar());
        values.put("lugar",horario.getHora());
        values.put("jornada",horario.getJornada());
        values.put("idHorario", 1 );

        db.insert("Horario", null, values);
        System.out.println("Guardado...");
        db.close();
}
    public List consultar2(){
        SQLiteDatabase db = getReadableDatabase();
        String reslutado="";
        List<String> lista1= new ArrayList<String>();
        Cursor cur=db.rawQuery("select lugar, hora, jornada"+sqlHorario, null);
        while (cur.moveToNext()){
            lista1.add(cur.getString(0)+"-"+ cur.getString(1));
        }
        cur.close();
        db.close();
        return(lista1);

    }



}